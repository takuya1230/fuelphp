<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>一覧画面</title>
	<style>
		body { margin: 40px; }
	</style>
	<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous">
    </script>
  <script>
      
      $(function (){
          $('input.vote').on('click', function(){
             var id = $(this).data('id');
             $.ajax({
                 url: "<?php echo Uri::create('api/vote.josn'); ?>",
                 type: "POST",
                 data: {id: id},
                 dataType: "json"
             }).done(function (data){
                 //追加
                 alert(data.message);
                 //画面再読み込み
                 location.reload();
             }).fail(function (data){
                 alert('処理に失敗しました');
             });
          });
      });
      
  </script>
</head>
<body>
一覧画面
<?php echo Html::anchor('vote/logout', 'ログアウト'); ?>

<div>
    <ul>
    <?php foreach($images as $img): ?>
        <li>
           <?php echo Asset::img($img['file_name']); ?>
            <span class="votes"></span><?php echo $img['votes']; ?></span>
            <input type="button" data-id="<?php echo $img['id']; ?>" class="vote" value="投票する">
        </li>
    <?php endforeach; ?>
    </ul>
</div>
</body>
</html>
