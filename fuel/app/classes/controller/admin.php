<?php

class Controller_Admin extends Controller {
    
    public function action_login()
    {
        
        if (Input::post('id') == 'admin' && Input::post('password') == 'pass') {
            Session::set('admin_login', true);
        }
        if (Session::get('admin_login') == true) {
            Response::redirect('admin/view');
        }
        
        return Response::forge(View::forge('admin/login'));
    }
    
    public function action_view()
    {
        // ログインしていなければログイン画面へ戻る
        if (Session::get('admin_login') != true) {
            Response::redirect('admin/login');
        }
        
        //データをロード
        $images = Model_Image::find('all');
        $data = array('images' => $images);
        
        return Response::forge(View::forge('admin/view', $data));
    }
    
    public function action_upload()
    {
        // ログインしていなければログイン画面へ戻る
        if (Session::get('admin_login') != true) {
            Response::redirect('admin/login');
        }
        
        if(Input::file('image')){
//        var_dump( Input::file('image') );
        $image = Input::file('image');
        move_uploaded_file($image['tmp_name'], 'assets/img/'. $image['name'] );
            
            $model_img = new Model_Image();
            $model_img->file_name = $image['name'];
            $model_img->info = '';
            $model_img->votes = 0;
            //保存処理
            $model_img->save();
        }
        
        return Response::forge(View::forge('admin/upload'));
    }
    
    public function action_logout()
    {
        // セッションの情報を消す
        Session::delete('admin_login');
        // ログイン画面にリダイレクト
        Response::redirect('admin/login');
    }
}








