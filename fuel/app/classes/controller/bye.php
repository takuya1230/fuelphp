<?php 

    class Controller_Bye extends Controller{
        
        public function action_index(){
            $data = array(
                "title" => "別れのあいさつ",
                "message" => "さよなら",
            );
            $view = View::forge('bye/index', $data);
            return Response::forge($view);
        }
        
    }
        
?>