<?php

    class Controller_Vote extends Controller{
        
        public function action_login(){
            //ログイン処理
            if(Input::post('id') != ''){
                if(Auth::login(Input::post('id'), Input::post('password'))){
                    Response::redirect('vote/view');
                }
            }
            
            return Response::forge(View::forge('vote/login'));
        }
        
        public function action_logout(){
            Auth::logout();
            Response::redirect('vote/login');
        }
        
        public function action_view(){
            //ログインができなかった時の処理
            if(! Auth::check()){
                Response::redirect('vote/login');
            }
            
            //データをロード
            $images = Model_Image::find('all');
            $data = array('images' => $images);

            return Response::forge(View::forge('vote/view', $data));
        }
        
    }

?>